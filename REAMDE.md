# TO DO

## Upgrading Helm chart

```shell

  helm upgrade --install spring-boot-demo -f ./ci-cd/helm/values.yaml -n demo ./ci-cd/helm

```

## Upgrading Helm chart

```shell

  helm install prometheus prometheus-community/prometheus -n monitoring --create-namespace
  helm upgrade --install prometheus prometheus-community/prometheus -n monitoring

```
