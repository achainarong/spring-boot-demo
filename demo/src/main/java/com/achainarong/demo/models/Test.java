package com.achainarong.demo.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

// Data is added so ToString, EqualsAndHashCode and Getters and Setters are generated
@Data
@RedisHash("Test")
public class Test {

    @Id @Indexed @ToString.Include
    private String id;
    
    @NonNull @ToString.Include
    private String Text;
}
