package com.achainarong.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.achainarong.demo.models.Test;

@Repository
public interface TestRepository extends CrudRepository<Test, String> { }