package com.achainarong.demo.controller.index;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
  @GetMapping("/")
  public String index() {
    return "Welcome to a demo project by Apiseg Chainarong you can access /api/info to get more information";
  }
}
