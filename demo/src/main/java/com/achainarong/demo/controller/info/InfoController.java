package com.achainarong.demo.controller.info;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

  @GetMapping("/api/info")
  public String info() {
    return "This project runs on Azure AKS with a gitlab pipeline";
  }
}
