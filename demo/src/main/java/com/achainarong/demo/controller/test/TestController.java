package com.achainarong.demo.controller.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.achainarong.demo.models.Test;
import com.achainarong.demo.repository.TestRepository;

@RestController
public class TestController {

  @Autowired
  private TestRepository testRepository;

  @GetMapping("/api/test")
  public ResponseEntity<Iterable<Test>> getAllTestObjects() {
    var testObjects = testRepository.findAll();
    return new ResponseEntity<Iterable<Test>>(testObjects, HttpStatus.OK);
  }

  @PostMapping("/api/test")
  public ResponseEntity<Test> createTestObject(@RequestBody Test test) {
    var createTestObject = testRepository.save(test);
    return new ResponseEntity<Test>(createTestObject, HttpStatus.CREATED);
  }
  
}
