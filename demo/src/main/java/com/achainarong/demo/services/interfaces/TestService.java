package com.achainarong.demo.services.interfaces;

import com.achainarong.demo.models.Test;

public interface TestService {

  public Iterable<Test> getAll();
  
  public Test add(Test test); 

  public Test update(Test test);

  public void delete(Test test);

  // public Test getById(String id);
}
