package com.achainarong.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.achainarong.demo.models.Test;
import com.achainarong.demo.services.interfaces.TestService;

import com.achainarong.demo.repository.TestRepository;

@CacheConfig(cacheNames = "testCache")
@Service
public class TestServiceImpl implements TestService {
  
  @Autowired
  private TestRepository testRepository;

  @Cacheable(cacheNames= "customers")
  @Override
  public Iterable<Test> getAll() {
    return this.testRepository.findAll();
  }

  @Override
  public Test add(Test test){
    return this.testRepository.save(test);
  }

  @Override
  public Test update(Test test){
    return this.testRepository.save(test);
  }

  @Override
  public void delete(Test test) {
    this.testRepository.delete(test);
  }

  // @Override
  // public Test getById(String id) {
  //   var converted = new RedisKeyValueAdapter();
  //   var convertedId =  
  //   return this.testRepository.findById(id);
  // }
  
}